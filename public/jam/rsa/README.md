#RSA Tools in an AMD package

This has been tested to work on the browser using JamJS, in node, and in couchdb lists and show functions.

It tries to provide a very simple interface to RSA public key encryption. The usual warnings about doing encryption in the browser apply.


How to generate a compatible public/private key:
================================================

Generate a public key certificate in mycert1-pub.pem and a new private key in mycert1.pem

```
openssl req   -x509 -nodes -days 365   -newkey rsa:1024 -keyout mycert1.pem -out mycert1-pub.pem
```
