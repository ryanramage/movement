define(['jquery', 'rsa', 'svg',  'text!img/icon.path'], function($, rsa, svg,  icon){
    return app;

    function app() {


        $(onload);
    }

    function onload() {

        $('button.encrypt').on('click', function(){
            var text = $('#text').val();
            try {
                encrypt(text);
            } catch(e) {console.log(e);}
            return false;
        });

        $('button.decrypt').on('click', function(){
            var text = $('#enc_text').val();
            try {
                decrypt(text);
            } catch(e) {console.log(e);}
            return false;
        });
    }

    function encrypt(text) {
        $.post('/encrypt', {'encrypt': text} , function(data, err){
            // comes back as binary
            $('.result-row').show();
            $('#result').text(data);
        });

    }

    function decrypt(text) {
        $.ajax({
            type: 'PUT',
            url: '/decrypt',
            data: {'decrypt' : text}
        }).done(function(msg){
            $('.result-row').show();
            $('#result').text(msg);
        });
    }

});