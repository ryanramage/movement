var fs = require('fs');


var decrypt_cmd_opts = ['smime',  '-decrypt', '-binary', '-inform', 'DEM', '-inkey', 'private.pem'];
var encrypt_cmd_opts = ['smime',  '-encrypt', '-binary', '-aes256', '-outform', 'DEM', 'public.pem'];

var spawn = require('child_process').spawn;


exports.encrypt = function(req, res) {

    if (!req.body || !req.body.encrypt) return exports.bad_request('please use a form post, with a upload files', req, res);
    res.setHeader('Content-Type', 'application/binary');

    exports._base64_encrypt_msg(req.body.encrypt, function(err, data){
        if (err) return exports.error(err, req, res);
        res.send(data);
    });
};


exports.decrypt = function(req, res) {
    if (!req.body || !req.body.decrypt) return exports.bad_request('please use a form put, with a upload files', req, res);
    res.setHeader('Content-Type', 'application/binary');


    exports._base64_decrypt_msg(req.body.decrypt, function(err, data){
        if (err) return exports.error(err, req, res);
        res.send(data);
    });
};

exports._base64_encrypt_msg = function(msg, callback) {
    exports._encrypt(msg, function(err, data){
        if (err) return callback(err);
        var d = data.toString("base64");
        return callback(null, d );
    });
};


exports._base64_decrypt_msg = function(base64msg, callback) {
    var msg = new Buffer(base64msg, 'base64');
    exports._decrypt(msg, callback);
};






exports._encrypt = function(msg, callback) {
    var final_cmd = encrypt_cmd_opts.slice(0, encrypt_cmd_opts.length);
    exports._openssl(final_cmd, msg, callback);
};

exports._decrypt = function(msg, callback) {
    // clone ane add the path
    var final_cmd = decrypt_cmd_opts.slice(0, decrypt_cmd_opts.length);
    exports._openssl(final_cmd, msg, callback);
};


exports._openssl = function(opts, msg, callback) {
    var ossl = spawn('openssl', opts);

    ossl.stdout.on('data', function(data){
        callback(null, data);
    });

    ossl.stderr.on('data', function(data){
        callback(data.toString());
    });

    if (msg) {
        ossl.stdin.write(msg);
        ossl.stdin.end();
    }
};



exports._encrypt_file = function(file, callback) {
    var final_cmd = encrypt_cmd_opts.slice(0, encrypt_cmd_opts.length);
    final_cmd.splice(3, 0, '-in');
    final_cmd.splice(4, 0, file);
    exports._openssl(final_cmd, null, callback);
};

exports._decrypt_file = function(file, callback) {
    // clone ane add the path
    var final_cmd = decrypt_cmd_opts.slice(0, decrypt_cmd_opts.length);
    final_cmd.splice(2, 0, '-in');
    final_cmd.splice(3, 0, file);
    exports._openssl(final_cmd, null, callback);
};



exports.public_key = function(req, res) {
    res.sendfile('public.pem');
};





exports.bad_request = function(msg, req, res) {
    res.status(400);
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({
        error: true,
        msg: msg
    }));
};

exports.error = function(msg, req, res) {

    // we get way too much stuff back from errors.
    var to_write = msg.toString().split('\n')[0];

    res.json(500, {
        error: true,
        msg: to_write
    });
};

