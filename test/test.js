var assert = require('assert');

var rsa = require('../routes/rsa');

describe('openssl', function(){
    it('should work both ways', function(cb){

        var msg = 'Hello from the world. No one shall see this';
        rsa._base64_encrypt_msg(msg, function(err, data){

            assert.ifError(err);
            rsa._base64_decrypt_msg(data, function(err2, data2){

                assert.ifError(err2);
                assert.equal(msg, data2);
                cb();

            });
        });
    });
});